﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Circustrein;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestDier
    {
        [TestMethod]
        public void TestDierToStringVlees()
        {
            //arrange
            Dier dier = new Dier(true, 5);


            //act & assert
            Assert.AreEqual("vlees 5", dier.ToString());
        }

        [TestMethod]
        public void TestDierToStringPlanten()
        {
            //arrange
            Dier dier = new Dier(false, 3);
            
            //act & assert
            Assert.AreEqual("planten 3", dier.ToString());
        }
    }
}
