﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Circustrein;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestAlgoritme
    {
        [TestMethod]
        public void TestAlgoritmeVleesetersEigenWagon()
        {
            //arrange
            Algoritme algoritme = new Algoritme();
            

            List<Dier> dieren = new List<Dier>();
            dieren.Add(new Dier(true, 5));
            dieren.Add(new Dier(true, 3));
            dieren.Add(new Dier(true, 1));

            //act
            algoritme.Bereken(dieren);

            //assert
            Assert.AreEqual(3, algoritme.Trein.Count);
        }

        [TestMethod]
        public void TestAlgoritmeAlsGeenVleeseterNieweWagon()
        {
            //arrange
            Algoritme algoritme = new Algoritme();
            List<Dier> dieren = new List<Dier>();

            //act
            algoritme.Bereken(dieren);

            //assert
            Assert.AreEqual(1, algoritme.Trein.Count);
        }

        [TestMethod]
        public void TestAlgoritmeVanAlles1()
        {
            //arrange
            Algoritme algoritme = new Algoritme();


            List<Dier> dieren = new List<Dier>();
            dieren.Add(new Dier(true, 5));
            dieren.Add(new Dier(true, 3));
            dieren.Add(new Dier(true, 1));
            dieren.Add(new Dier(false, 5));
            dieren.Add(new Dier(false, 3));
            dieren.Add(new Dier(false, 1));

            //act
            algoritme.Bereken(dieren);

            //assert
            Assert.AreEqual(4, algoritme.Trein.Count);

        }
    }
}
