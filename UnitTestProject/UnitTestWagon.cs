﻿using System;
using System.Collections.Generic;
using Circustrein;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestWagon
    {
        [TestMethod]
        public void TestVoegDierToe()
        {
            //arrange
            List<Dier> list = new List<Dier>();
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(true, 5);

            //act
            wagon.VoegDierToe(dier);

            //assert
            Assert.AreEqual(1, wagon.Dieren.Count);
            Assert.AreEqual(5, wagon.Punten);
        }

        [TestMethod]
        public void TestPastCapaciteit()
        {
            //arrange
            List<Dier> list = new List<Dier>();
            Wagon wagon = new Wagon(6, list);
            Dier dier = new Dier(false, 5);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(false, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegGrotePlanteterToePositief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            list.Add(new Dier(true, 3));
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 5);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(true, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegGrotePlanteterToeNegatief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            list.Add(new Dier(true, 5));
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 5);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(false, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegMiddelPlanteterToePositief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            list.Add(new Dier(true, 1));
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 3);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(true, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegMiddelPlanteterToeNegatief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            list.Add(new Dier(true, 4));
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 3);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(false, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegKleinePlanteterToePositief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 1);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(true, wagon.boolpast);
        }

        [TestMethod]
        public void TestVoegKleinePlanteneterToeNegatief()
        {
            //arrange
            List<Dier> list = new List<Dier>();

            list.Add(new Dier(true, 1));
            Wagon wagon = new Wagon(0, list);
            Dier dier = new Dier(false, 1);

            //act
            wagon.Past(dier);

            //assert
            Assert.AreEqual(false, wagon.boolpast);
        }
    }
}
