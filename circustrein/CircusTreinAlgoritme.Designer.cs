﻿namespace Circustrein
{
    partial class CircusTreinAlgoritme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.cmb_eten = new System.Windows.Forms.ComboBox();
            this.lbl_eten = new System.Windows.Forms.Label();
            this.lbl_punten = new System.Windows.Forms.Label();
            this.cmb_punten = new System.Windows.Forms.ComboBox();
            this.listbox_dieren = new System.Windows.Forms.ListBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_dieren = new System.Windows.Forms.Label();
            this.lbl_wagon = new System.Windows.Forms.Label();
            this.listbox_wagondieren = new System.Windows.Forms.ListBox();
            this.listbox_wagons = new System.Windows.Forms.ListBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 45);
            this.button1.TabIndex = 7;
            this.button1.Text = "Toevoegen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmb_eten
            // 
            this.cmb_eten.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_eten.FormattingEnabled = true;
            this.cmb_eten.Items.AddRange(new object[] {
            "Vlees",
            "Planten"});
            this.cmb_eten.Location = new System.Drawing.Point(24, 34);
            this.cmb_eten.Name = "cmb_eten";
            this.cmb_eten.Size = new System.Drawing.Size(121, 21);
            this.cmb_eten.TabIndex = 10;
            // 
            // lbl_eten
            // 
            this.lbl_eten.AutoSize = true;
            this.lbl_eten.Location = new System.Drawing.Point(21, 9);
            this.lbl_eten.Name = "lbl_eten";
            this.lbl_eten.Size = new System.Drawing.Size(32, 13);
            this.lbl_eten.TabIndex = 11;
            this.lbl_eten.Text = "Eten:";
            // 
            // lbl_punten
            // 
            this.lbl_punten.AutoSize = true;
            this.lbl_punten.Location = new System.Drawing.Point(21, 68);
            this.lbl_punten.Name = "lbl_punten";
            this.lbl_punten.Size = new System.Drawing.Size(44, 13);
            this.lbl_punten.TabIndex = 12;
            this.lbl_punten.Text = "Punten:";
            // 
            // cmb_punten
            // 
            this.cmb_punten.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_punten.FormattingEnabled = true;
            this.cmb_punten.Items.AddRange(new object[] {
            "1",
            "3",
            "5"});
            this.cmb_punten.Location = new System.Drawing.Point(24, 98);
            this.cmb_punten.Name = "cmb_punten";
            this.cmb_punten.Size = new System.Drawing.Size(121, 21);
            this.cmb_punten.TabIndex = 13;
            // 
            // listbox_dieren
            // 
            this.listbox_dieren.FormattingEnabled = true;
            this.listbox_dieren.Location = new System.Drawing.Point(178, 18);
            this.listbox_dieren.Name = "listbox_dieren";
            this.listbox_dieren.Size = new System.Drawing.Size(120, 173);
            this.listbox_dieren.TabIndex = 14;
            // 
            // btn_calc
            // 
            this.btn_calc.Font = new System.Drawing.Font("Old English Text MT", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_calc.Location = new System.Drawing.Point(24, 216);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(121, 53);
            this.btn_calc.TabIndex = 15;
            this.btn_calc.Text = "Calculeer";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_dieren
            // 
            this.lbl_dieren.AutoSize = true;
            this.lbl_dieren.Location = new System.Drawing.Point(499, 1);
            this.lbl_dieren.Name = "lbl_dieren";
            this.lbl_dieren.Size = new System.Drawing.Size(41, 13);
            this.lbl_dieren.TabIndex = 19;
            this.lbl_dieren.Text = "Dieren:";
            // 
            // lbl_wagon
            // 
            this.lbl_wagon.AutoSize = true;
            this.lbl_wagon.Location = new System.Drawing.Point(329, 2);
            this.lbl_wagon.Name = "lbl_wagon";
            this.lbl_wagon.Size = new System.Drawing.Size(50, 13);
            this.lbl_wagon.TabIndex = 18;
            this.lbl_wagon.Text = "Wagons:";
            // 
            // listbox_wagondieren
            // 
            this.listbox_wagondieren.FormattingEnabled = true;
            this.listbox_wagondieren.Location = new System.Drawing.Point(499, 18);
            this.listbox_wagondieren.Name = "listbox_wagondieren";
            this.listbox_wagondieren.Size = new System.Drawing.Size(134, 251);
            this.listbox_wagondieren.TabIndex = 17;
            // 
            // listbox_wagons
            // 
            this.listbox_wagons.FormattingEnabled = true;
            this.listbox_wagons.Location = new System.Drawing.Point(332, 18);
            this.listbox_wagons.Name = "listbox_wagons";
            this.listbox_wagons.Size = new System.Drawing.Size(133, 251);
            this.listbox_wagons.TabIndex = 16;
            this.listbox_wagons.SelectedIndexChanged += new System.EventHandler(this.listbox_wagons_SelectedValueChanged);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(178, 216);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(120, 53);
            this.btn_clear.TabIndex = 20;
            this.btn_clear.Text = "Clear All";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // CircusTreinAlgoritme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 281);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.lbl_dieren);
            this.Controls.Add(this.lbl_wagon);
            this.Controls.Add(this.listbox_wagondieren);
            this.Controls.Add(this.listbox_wagons);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.listbox_dieren);
            this.Controls.Add(this.cmb_punten);
            this.Controls.Add(this.lbl_punten);
            this.Controls.Add(this.lbl_eten);
            this.Controls.Add(this.cmb_eten);
            this.Controls.Add(this.button1);
            this.Name = "CircusTreinAlgoritme";
            this.Text = "Dier toevoegen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmb_eten;
        private System.Windows.Forms.Label lbl_eten;
        private System.Windows.Forms.Label lbl_punten;
        private System.Windows.Forms.ComboBox cmb_punten;
        private System.Windows.Forms.ListBox listbox_dieren;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_dieren;
        private System.Windows.Forms.Label lbl_wagon;
        private System.Windows.Forms.ListBox listbox_wagondieren;
        private System.Windows.Forms.ListBox listbox_wagons;
        private System.Windows.Forms.Button btn_clear;
    }
}

