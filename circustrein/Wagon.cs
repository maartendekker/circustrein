﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circustrein
{
    public class Wagon
    {
        private readonly int Capacitiet = 10; 

        public int Punten;

        public List<Dier> Dieren;

        public Wagon(int punten, List<Dier>dieren)
        {
            this.Punten = punten;
            this.Dieren = dieren;
        }

        public bool boolpast;

        public bool Past(Dier dier)

         {
            if ((Punten + dier.Punten <= Capacitiet) == false) // check if there is room for the animal
            {
                boolpast = false;
            }
            else
            {
                if (dier.Punten == 5) // check if a 5 point herbivoor can be added
                {
                    if (Dieren.Any(f => f.Carnivoor == true && f.Punten == 5))
                    {
                        boolpast = false;
                    }
                    else
                    {
                        boolpast = true;
                    }
                }
                if (dier.Punten == 3) // check if a 3 point herbivoor can be added
                {
                    if (Dieren.Any(f => f.Carnivoor == true && f.Punten >= 3))
                    {
                        boolpast = false;
                    }
                    else
                    {
                        boolpast = true;
                    }
                }
                if (dier.Punten == 1) // check if a 1 point herbivoor can be added
                {
                    if(Dieren.Any(f => f.Carnivoor == true))
                    {
                        boolpast = false;
                    }
                    else
                    {
                        boolpast = true;
                    }
                }

            }

            return boolpast;
        }

        public void VoegDierToe(Dier dier) // method that adds animal to wagon and updates the score.
        {
            Dieren.Add(dier);
            Punten = (Punten + dier.Punten);
        }


        
    }
}
