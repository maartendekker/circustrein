﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Circustrein
{
    public partial class CircusTreinAlgoritme : Form
    {
        public List<Dier> dieren;
        public CircusTreinAlgoritme()
        {
            InitializeComponent();
            this.CenterToScreen();
            dieren = new List<Dier>();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            
            if (cmb_eten.SelectedItem.ToString() == "Vlees")
            {
                dieren.Add(new Dier(true, Convert.ToInt32(cmb_punten.SelectedItem)));
            }
            else
            {
                dieren.Add(new Dier(false, Convert.ToInt32(cmb_punten.SelectedItem)));
            }

            listbox_dieren.Items.Clear();
            foreach (Dier dier in dieren)
            {
                listbox_dieren.Items.Add(dier.ToString());
            }

        }

        public void btn_calc_Click(object sender, EventArgs e)
        {
            Algoritme algoritme = new Algoritme();
            List<Wagon> trein = new List<Wagon>();
            trein = algoritme.Bereken(dieren);

            foreach (Wagon wagon in trein)
            {
                listbox_wagons.Items.Add(wagon);
            }
        }

        private void listbox_wagons_SelectedValueChanged(object sender, EventArgs e)
        {
            listbox_wagondieren.Items.Clear();

            List<Dier> wagonDieren = new List<Dier>();
            Wagon selectedWagon = (Wagon)listbox_wagons.SelectedItem;

            foreach (Dier dier in selectedWagon.Dieren)
            {
                listbox_wagondieren.Items.Add(dier);
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
            this.Close(); //to turn off current app
        }
    }
}
