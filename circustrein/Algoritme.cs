﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circustrein
{
    public class Algoritme
    {
        public List<Wagon> Trein = new List<Wagon>();

        public List<Wagon> Bereken(List<Dier> dieren)
        {
            List<Dier> tosort = new List<Dier>();
            tosort = this.WagonForEachCarnivoor(dieren).OrderBy(o => o.Punten).ToList(); // order list of animals by points
            this.AddAnimalToWagon(tosort);
            return Trein;
        }

        public List<Dier> WagonForEachCarnivoor(List<Dier> dieren)
        {
            if (dieren.Any(f => f.Carnivoor == true)) // Make new wagon for every carnivoor
            {
                foreach (Dier dier in dieren.Where(f => f.Carnivoor == true))
                {
                    List<Dier> diers = new List<Dier>();
                    diers.Add(dier);
                    Trein.Add(new Wagon(dier.Punten, diers));
                }
            }
            else // if there are no carnivoors, initiate empty wagon
            {
                Wagon initialwagon = new Wagon(0, new List<Dier>());
                Trein.Add(initialwagon);
            }

            dieren.RemoveAll(f => f.Carnivoor == true); // remove all carnivoors from list

            return dieren;
        }

        public void AddAnimalToWagon(List<Dier> tosort)
        {
            List<Wagon> passsendewagons = new List<Wagon>();

            foreach (Dier dier in tosort) // check each wagon if the animal that has to be added can fit in
            {
                foreach (Wagon wagon in Trein)
                {
                    if (wagon.Past(dier) == true)
                    {
                        passsendewagons.Add(wagon);
                    }
                }

                if (passsendewagons.Count != 0) // if there is a wagon wich the animal can be added to, insert.
                {
                    passsendewagons = passsendewagons.OrderByDescending(o => o.Punten).ToList();
                    Trein.Find(f => f == passsendewagons[0]).VoegDierToe(dier);
                }
                else // else create an new wagon and add the animal to it.
                {
                    List<Dier> diers = new List<Dier>();
                    diers.Add(dier);
                    Trein.Add(new Wagon(dier.Punten, diers));
                }

                passsendewagons.Clear();

            }
        }

    }
}
