﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circustrein
{
    public class Dier
    {
        public bool Carnivoor;

        public int Punten;

        public Dier(bool Carnivoor, int Punten)
        {
            this.Carnivoor = Carnivoor;
            this.Punten = Punten;
        }

        public override string ToString()
        {
            string eten;
            if (Carnivoor == true)
            {
                eten = "vlees";
            }
            else
            {
                eten = "planten";
            }

            string punten = Convert.ToString(Punten);

            return eten + " " + punten;
        }

    }
}
